﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");

using System;
using model;

  class MainProgram
  {
    static void Main(string[] args)
    {
        //Ärzte
        Doctor doctor_1 = new Doctor(1, "Glaucomaflecken", 1975, "Dr. med.");
        Doctor doctor_2 = new Doctor(2, "Mike", 1968, "Dr. med.");
        Doctor doctor_3 = new Doctor(3, "Fischer", 1999, "Dr. med.");
        Doctor[] doctors = new Doctor[] {doctor_1, doctor_2, doctor_3};

        //Patienten
        Patient patient_1 = new Patient (1, "Xaila", 1970, 80.0F, doctor_1);
        Patient patient_2 = new Patient (2, "Xander", 1940, 75.2F, doctor_1);
        Patient patient_3 = new Patient (3, "Xiang", 19, 95.9F, doctor_1);
        Patient patient_4 = new Patient (4, "Xylia", 1918, 60.1F, doctor_2);
        Patient patient_5 = new Patient (5, "Xiola", 1958, 101.3F, doctor_2);
        Patient patient_6 = new Patient (6, "Xiao Hong", 1967, 94.7F, doctor_3);
        Patient patient_7 = new Patient (7, "Xerxes", 1964, 79.0F, doctor_3);
        Patient[] patients = new Patient[] {patient_1, patient_2, patient_3, patient_4, patient_5, patient_6, patient_7};

        Console.WriteLine("----------------");
        Console.WriteLine("Ärzte im System");
        Console.WriteLine("----------------");
        Console.WriteLine("ID, Titel Name");

        foreach (Doctor doctor in doctors)
        {
            System.Console.Write(doctor.getUid());
            System.Console.Write(", ");
            System.Console.Write(doctor.getTitle());
            System.Console.Write(" ");
            System.Console.Write(doctor.getName());
            Console.WriteLine("");
        }

        Console.WriteLine("----------------");
        Console.WriteLine("Patienten im System");
        Console.WriteLine("----------------");
        Console.WriteLine("ID, Name");

        foreach (Patient patient in patients)
        {
            System.Console.Write(patient.getUid());
            System.Console.Write(", ");
            System.Console.Write(patient.getName());
            Console.WriteLine("");
        }

        Console.WriteLine("----------------");
        Console.WriteLine("Wollen Sie einen Arzt oder einen Patienten auswählen? ");
        string choice = Console.ReadLine();
        if (choice.ToUpper() == "ARZT")
        {
            Console.WriteLine("----------------");
            Console.WriteLine("Geben Sie die UID des Arztes ein: ");
            string uid = Console.ReadLine();
            bool idFound = false;
            foreach (Doctor doctor in doctors)
            {
                if (Equals(doctor.getUid(), int.Parse(uid)))
                {
                    idFound = true;
                    Console.WriteLine("----------------");
                    Console.WriteLine(doctor.getTitle() + " " + doctor.getName()+ " (ID: " + doctor.getUid() + ")");
                    Console.WriteLine("Alter: " + doctor.getAge() + " Jahre");
                }
            }
            if (!idFound){
                Console.WriteLine("----------------");
                Console.WriteLine("Fehler: Diese ID ist nicht bekannt.");
            }
        }
        if (choice.ToUpper() == "PATIENT")
        {
            Console.WriteLine("----------------");
            Console.WriteLine("Geben Sie die UID des Patienten ein: ");
            string uid = Console.ReadLine();
            bool idFound = false;
            foreach (Patient patient in patients)
            {
                if (Equals(patient.getUid(), int.Parse(uid)))
                {
                    idFound = true;
                    Console.WriteLine("----------------");
                    Console.WriteLine(patient.getName() + " (ID: " + patient.getUid() + ")");
                    Console.WriteLine("Alter: " + patient.getAge() + " Jahre");
                    Console.WriteLine("Gewicht: " + patient.getWeight() + "kg");
                    Console.WriteLine("Behandelnder Arzt: " + patient.getAssignedDoctor().getTitle() + " " + patient.getAssignedDoctor().getName());
                    Console.WriteLine("Risikofaktor: " + patient.getRiskFactor());
                }
            }
            if (!idFound){
                Console.WriteLine("----------------");
                Console.WriteLine("Fehler: Diese ID ist nicht bekannt.");
            }
        }

    }
  }
