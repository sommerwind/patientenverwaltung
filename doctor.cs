
namespace model
{
    class Doctor
{

        private int uid;
        private string name;
        private int yearOfBirth;
        private string title;

        public Doctor (     int t_uid, 
                    string t_name,  
                    int t_YearOfBirth,  
                    string t_title)
    {
        this.uid = t_uid;
        this.name = t_name;
        this.yearOfBirth = t_YearOfBirth;
        this.title = t_title;
    }

        public int getUid()
        {
            return this.uid;
        }

        public void setUid(int uid)
        {
            this.uid = uid;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public int getYearOfBirth()
        {
            return this.yearOfBirth;
        }

        public void setYearOfBirth(int yearOfBirth)
        {
            this.yearOfBirth = yearOfBirth;
        }

        public String getTitle()
        {
            return this.title;
        }

        public void setTitle(string title)
        {
            this.title = title;
        }

        public int getAge ()
        {
            return 2022 - this.yearOfBirth;
        }
}
}
