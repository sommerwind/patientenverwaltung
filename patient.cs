namespace model
{
class Patient
{

        private int uid;
        private string name;
        private int yearOfBirth;
        private float weight;
        private Doctor assignedDoctor;

        public int getUid()
        {
            return this.uid;
        }

        public void setUid(int uid)
        {
            this.uid = uid;
        }

        public string getName()
        {
            return this.name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public int getYearOfBirth()
        {
            return this.yearOfBirth;
        }

        public void setYearOfBirth(int yearOfBirth)
        {
            this.yearOfBirth = yearOfBirth;
        }

        public float getWeight()
        {
            return this.weight;
        }

        public void setWeight(float weight)
        {
            this.weight = weight;
        }

        public Doctor getAssignedDoctor()
        {
            return this.assignedDoctor;
        }

        public void setAssignedDoctor(Doctor assignedDoctor)
        {
            this.assignedDoctor = assignedDoctor;
        }


        public Patient (int t_uid, 
                    string t_name,  
                    int t_YearOfBirth,  
                    float t_weight,  
                    Doctor t_assignedDoctor)
    {
        this.uid = t_uid;
        this.name = t_name;
        this.yearOfBirth = t_YearOfBirth;
        this.weight = t_weight;
        this.assignedDoctor = t_assignedDoctor;
    }

    public float getRiskFactor () 
    {
        int age = this.getAge();
        return 1.234F * this.weight * age;
    }

    public int getAge ()
    {
        return 2022 - this.yearOfBirth;
    }
    
}

}